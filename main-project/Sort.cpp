#include "Sort.h"

void merge(productCatalog* arr, int first, int last, bool (criteria)(productCatalog a, productCatalog b)) {
	int middle, start, final, j;
	productCatalog* mas = new productCatalog  [100];
	middle = (first + last) / 2;  //��������� �������� ��������
	start = first;                //������ ����� �����
	final = middle + 1;           //������ ������ �����
	for (j = first; j <= last; j++)  //��������� �� ������ �� �����
		if ((start <= middle) && ((final > last) || criteria(arr[final], arr[start]))) {
			mas[j] = arr[start];
			start++;
		}
		else {
			mas[j] = arr[final];
			final++;
		}
}
void mergeSort(productCatalog* arr, int first, int last, bool (criteria)(productCatalog a, productCatalog b)) {
	if (first < last) {
		mergeSort(arr, first, (first + last) / 2, criteria);  //���������� ����� �����
		mergeSort(arr, (first + last) / 2 + 1, last, criteria);  //���������� ������ �����
		merge(arr, first, last, criteria);  //������� ���� ������
	}
}	




void selectSort(productCatalog* arrPtr, int length, bool (criteria)(productCatalog a, productCatalog b)) { //���������
	for (int i = 0; i < length; ++i) {
		productCatalog temp = arrPtr[0];
		for (int j = i + 1; j < length; ++j) {
			if (criteria(arrPtr[i], arrPtr[j])) {   //1) ���� first < second  
				temp = arrPtr[j];					 //2) ���� i > j , ������ ������� ( 1 0 -> 0 1 )
				arrPtr[j] = arrPtr[i];
				arrPtr[i] = temp;      
				
				//swap(arrPtr[i], arrPtr[j]); //��� ���-�� ������� : ���� ����� < ������� swap (��������)
			}								///��� �������� ���������: ���� ������ ������ ������� ������� � ���������� 
		}									//
	}

}

bool comparsionQuantity(productCatalog a, productCatalog b) {  //1-�� �������� sort//���������� ���-�� �������  ����� ����� ��������
	return a.tovarQuantity < b.tovarQuantity;
}

bool comparsionCategory(productCatalog a, productCatalog b) {  //2-�� �������� sort 
	enum categories {
		BitovayaTehnika, Elektronika, Knigi, Nedvigimost, Promtovari
	};
	int rightnum = 0, leftnum = 0;
	if (a.tovarCategory == "BitovayaTehnika") {
		leftnum = BitovayaTehnika;
	}
	else if (a.tovarCategory == "Elektronika") {
		leftnum = Elektronika;
	}
	else if (a.tovarCategory == "Knigi") {
		leftnum = Knigi;
	}
	else if (a.tovarCategory == "Nedvigimost") {
		leftnum = Nedvigimost;
	}
	else if (a.tovarCategory == "Promtovari") {
		leftnum = Promtovari;
	}
	if (b.tovarCategory == "BitovayaTehnika") {
		rightnum = BitovayaTehnika;
	}
	else if (b.tovarCategory == "Elektronika") {
		rightnum = Elektronika;
	}
	else if (b.tovarCategory == "Knigi") {
		rightnum = Knigi;
	}
	else if (b.tovarCategory == "Nedvigimost") {
		rightnum = Nedvigimost;
	}
	else if (b.tovarCategory == "Promtovari") {
		rightnum = Promtovari;
	}
	if (a.tovarCategory == b.tovarCategory) {
		return a.tovarCost < b.tovarCost;
		
	} 

	return leftnum > rightnum;
								  	
}












/*void merge(int array[], int const left, int const mid, int const right)
{
	auto const subArrayOne = mid - left + 1;
	auto const subArrayTwo = right - mid;

	// Create temp arrays
	auto* leftArray = new int[subArrayOne],
		* rightArray = new int[subArrayTwo];

	// Copy data to temp arrays leftArray[] and rightArray[]
	for (auto i = 0; i < subArrayOne; i++)
		leftArray[i] = array[left + i];
	for (auto j = 0; j < subArrayTwo; j++)
		rightArray[j] = array[mid + 1 + j];

	auto indexOfSubArrayOne
		= 0, // Initial index of first sub-array
		indexOfSubArrayTwo
		= 0; // Initial index of second sub-array
	int indexOfMergedArray
		= left; // Initial index of merged array

	// Merge the temp arrays back into array[left..right]
	while (indexOfSubArrayOne < subArrayOne
		&& indexOfSubArrayTwo < subArrayTwo) {
		if (leftArray[indexOfSubArrayOne]
			<= rightArray[indexOfSubArrayTwo]) {
			array[indexOfMergedArray]
				= leftArray[indexOfSubArrayOne];
			indexOfSubArrayOne++;
		}
		else {
			array[indexOfMergedArray]
				= rightArray[indexOfSubArrayTwo];
			indexOfSubArrayTwo++;
		}
		indexOfMergedArray++;
	}
	// Copy the remaining elements of
	// left[], if there are any
	while (indexOfSubArrayOne < subArrayOne) {
		array[indexOfMergedArray]
			= leftArray[indexOfSubArrayOne];
		indexOfSubArrayOne++;
		indexOfMergedArray++;
	}
	// Copy the remaining elements of
	// right[], if there are any
	while (indexOfSubArrayTwo < subArrayTwo) {
		array[indexOfMergedArray]
			= rightArray[indexOfSubArrayTwo];
		indexOfSubArrayTwo++;
		indexOfMergedArray++;
	}
	delete[] leftArray;
	delete[] rightArray;
}

// begin is for left index and end is
// right index of the sub-array
// of arr to be sorted 

void mergeSort(int array[], int const begin, int const end)
{
	if (begin >= end)
		return; // Returns recursively

	auto mid = begin + (end - begin) / 2;
	mergeSort(array, begin, mid);
	mergeSort(array, mid + 1, end);
	merge(array, begin, mid, end);
} */

