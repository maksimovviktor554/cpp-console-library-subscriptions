#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include "Product _catalog.h"
using namespace std;
bool comparsionQuantity(productCatalog a, productCatalog b);
bool comparsionCategory(productCatalog a, productCatalog b);
void selectSort(productCatalog* arrPtr, int lengthArr, bool (criteria)(productCatalog a, productCatalog b));
void merge(productCatalog* arr, int first, int last, bool (criteria)(productCatalog a, productCatalog b));
void mergeSort(productCatalog* arr, int first, int last, bool (criteria)(productCatalog a, productCatalog b));
/*void merge(int array[], int const left, int const mid, int const right);
void mergeSort(int array[], int const begin, int const end);*/